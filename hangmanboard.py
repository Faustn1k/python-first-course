import sys

HANGMAN_PHOTOS = {
0: """
x-------x""",
1: """
x-------x
|
|
|
|
|""",
2: """
x-------x
|       |
|       0
|
|
|""",
3: """
x-------x
|       |
|       0
|       |
|
|""",
4: """
x-------x
|       |
|       0
|      /|\ 
|
|""",
5: """
x-------x
|       |
|       0
|      /|\ 
|      /
|""",
6: """
x-------x
|       |
|       0
|      /|\ 
|      / \ 
|"""}

def is_valid_input(letter_guessed):
    if len(letter_guessed) != 1 or letter_guessed.isalpha() == False:
        return False
    return True

def check_valid_input(letter_guessed, old_letters_guessed):
    ''' checks if the letter has not been guessed, if its a true letter in the alphabet and if its only one letter.
    :param letter_guessed: letter guessed
    :type letter_guessed:  string
    :param old_letters_guessed: letters guessed
    :type old_letters_guessed:  array of chars
    :return: true if letter_guessed meets the corresponding conditions
    '''
    if len(letter_guessed) != 1 or letter_guessed.isalpha() == False or letter_guessed in old_letters_guessed:
        return False
    return True


def try_update_letter_guessed(letter_guessed, old_letters_guessed):
    ''' updates the  old_letters_guessed array with a new letter guessed if the guess is valid
    :param letter_guessed: letter guessed
    :type letter_guessed:  string
    :param old_letters_guessed: letters guessed
    :type old_letters_guessed:  array of chars
    :return: true if adding was successful
    '''
    do_if = check_valid_input(letter_guessed, old_letters_guessed)
    if do_if == True:
        old_letters_guessed.append(letter_guessed)
        print_guessed(old_letters_guessed)
        return True
    else:
        return False

def print_guessed(old_letters_guessed):
    ''' prints the letters guessed so far 
    :param old_letters_guessed: letters guessed
    :type old_letters_guessed:  array of chars
    '''
    string = sorted(old_letters_guessed, key=str.lower)
    string  = " -> ".join(string)
    print(string)

def show_hidden_word(secret_word, old_letters_guessed):
    s =  "_ " * len(secret_word)
    indexes = []
    for i in range(len(secret_word)):
        for letter in old_letters_guessed:
            if secret_word[i] == letter:
                s= s[:i * 2] + letter + s[i * 2 + 1:] 
    return s


def check_win(secret_word, old_letters_guessed):
    ''' checks if the old letters guessed are equal together to the secret word.
    :param secret_word: word to guess
    :type secret_word:  string
    :param old_letters_guessed: letters guessed
    :type old_letters_guessed:  array of chars
    :return: True or false
    '''
    s = show_hidden_word(secret_word, old_letters_guessed)
    secret_word = secret_word.replace(" ", "")
    s = s.replace(" ","")
  
    return s == secret_word

def print_hangman(num_of_tries):
    ''' prints the right structure of the hangman based on the number of tries occured
    :param num_of_tries: number of tries
    :type file_name:  int
    '''
    output =""
    for line in HANGMAN_PHOTOS[num_of_tries]:
        output += line 
    print(output)
    #return output


def choose_word(file_name, n):
    ''' return the number of unique words in the file and requested word.
    :param file_name: name of text file containing lists of words (may include repetitions)
    :type file_name:  string
    :param n: index of the word in a list of unique words in the file [1,n_words]
    :type n:  int
    :return: number of unique words in the file + the chosen word
    :rtype: tuple
    '''
    word_map = {}
    print(file_name)
    with open(file_name, 'r') as input_file:
        word_list = input_file.read().split()
        if len(word_list) < n:
            sys.exit()
        for word in word_list:
            word_map[word] = 0 
    unique_word_list = list(word_map.keys())
    index = (n -1 ) % len(word_list)
    return len(unique_word_list) , word_list[index]

def print_hang():
    HANGMAN_ASCII_ART = """ 
     _    _
    | |  | |
    | |__| | __ _ _ __   __ _ _ __ ___   __ _ _ __
    |  __  |/ _' | '_ \ / _' | '_ ' _ \ / _' | '_ \\
    | |  | | (_| | | | | (_| | | | | | | (_| | | | |
    |_|  |_|\__,_|_| |_|\__, |_| |_| |_|\__,_|_| |_|
                         __/ |
                        |___/"""
    print(HANGMAN_ASCII_ART)
    MAX_TRIES = 6
    print("Max tries: " + str(MAX_TRIES))


def main():
    print_hang()
    #2
    path = input("Please enter a path for the words file: ")
    index = input("Please enter an index of a word in the file: ")
    temp, secret_word  = choose_word(path, int(index))
    old_letters_guessed = list()
    print("STARTING GAME!")
    # stage 3
    MAX_TRIES = 6
    num_tries = 0
    while num_tries <= MAX_TRIES :
        if num_tries == MAX_TRIES:
            print("YOU LOST!")
            break
        hidden_word = show_hidden_word(secret_word, old_letters_guessed)
        print(hidden_word)
        if check_win(secret_word, old_letters_guessed):
            print("YOU GUESSED THE WORD!")
            break
        
        while True:
            guess_letter = input("Guess a letter: ")
            is_valid = try_update_letter_guessed(guess_letter, old_letters_guessed)
            if not is_valid:
                print("X")
                continue
            elif guess_letter.lower() not in secret_word.lower():
                num_tries +=1
                print("WRONG GUESS {0}/{1}".format(num_tries,MAX_TRIES))
                print_hangman(num_tries)
                break
            else:
                print("Nice guess!")
                break

    print("the word was {0}".format(secret_word))

if __name__ == "__main__":
    main()
