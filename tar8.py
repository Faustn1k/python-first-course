from datetime import date


def get_item_price(x):
    return x[1]

def sort_item_prices(list_of_tuples):
    ''' Sort tuples of ('item', price) from lowest price to highest price
        :param arg1: list of tuple items (;item name', item_price)

    '''
    list_of_tuples.sort(key=get_item_price)
    
products = [('milk', '5.5'), ('candy', '2.5'), ('bread', '9.0')]
sort_item_prices(products)
print(products)


def mult_tuple(t1, t2):
    ''' function returns all the pairs possible from the tuples
        :param arg1: list of tuples
        :param arg2: list of tuples

    '''
    pairs = []
    for item1 in t1:
        for item2 in t2:
            pairs.append((item1, item2))
            pairs.append((item2, item1))
    return tuple(pairs)


first_tuple = (1, 2, 3)
second_tuple = (4, 5, 6)
print(mult_tuple(first_tuple, second_tuple))


def sort_anagrams(words):
    ''' Sort tuples by anagrams
        :param arg1: list of words

    '''
    anagrams_dict = {}
    for word in words:
        sorted_word = ''.join(sorted(word))
        if sorted_word in anagrams_dict:
            anagrams_dict[sorted_word].append(word)
        else:
            anagrams_dict[sorted_word] = [word]
    return list(anagrams_dict.values())


list_of_words = ['deltas', 'retainers', 'desalt', 'pants', 'slated', 'generating', 'ternaries', 'smelters', 'termless', 'salted', 'staled', 'greatening', 'lasted', 'resmelts']
print(sort_anagrams(list_of_words))



# 8.3.4

def inverse_dict(original_dict):
    """ return a new dictionary with original values as key and list of original keys as value
        :param original_dict: dictionary
    """
    out_dict = dict()
    for original_key in original_dict:
        original_value =  original_dict[original_key]
        if original_value in out_dict.keys():
            out_dict[original_value] += [original_key]
            out_dict[original_value].sort()
        else:
            out_dict[original_value] = [original_key]

    return out_dict


course_dict = {'I': 3, 'love': 3, 'self.py!': 2}
print(inverse_dict(course_dict))



"""
mariah_carey = {
"first_name": "mariah",
"last_name": "Carey",
"birth_date": "27.03.1970",
"hobbies": ["Sing", "Compose", "Act"]
}

input_n = int(input("please enter a number 1-7: "))
birth_date_parts = mariah_carey["birth_date"].split(".")

if input_n == 1:
    print(mariah_carey["last_name"])
elif input_n == 2:
    birth_month = mariah_carey["birth_date"].split(".")[1]
    print(birth_month)
elif input_n == 3:
    num_hobbies = len(mariah_carey["hobbies"])
    print(num_hobbies)
elif input_n == 4:
    last_hobby = mariah_carey["hobbies"][-1]
    print(last_hobby)
elif input_n == 5:
    mariah_carey["hobbies"].append("Cooking")
elif input_n == 6:
    birth_date_tuple = (int(birth_date_parts[0]), int(birth_date_parts[1]), int(birth_date_parts[2]))
    print(birth_date_tuple)
elif input_n == 7:
    today = date.today()
    birth_year = int(birth_date_parts[2])
    age = today.year - birth_year - ((today.month, today.day) < (int(birth_date_parts[1]), int(birth_date_parts[0])))
    mariah_carey["age"] = age
    print(mariah_carey["age"])
"""

def count_chars(my_str):
    """ counts repetitions of letters and puts it in a dict 
        :arg1 : string
    """
    char_dict = {}
    for char in my_str:
        if char != " ":
            if char in char_dict:
                char_dict[char] += 1
            else:
                char_dict[char] = 1
    return char_dict


magic_str = "abra cadabra"
print(count_chars(magic_str))