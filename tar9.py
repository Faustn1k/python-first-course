def are_files_equal(file1, file2):
    content1 = ""
    content2 = ""
    with open(file1,'r') as file1_r:
        content1 = file1_r.read()
    with open(file2,'r') as file2_r:
        content2 = file2_r.read()
    return content1 == content2

def read_file(file_path):
    with open(file_path, 'r') as file:
        lines = file.readlines()
        lines = [line.strip() for line in lines] # מחיקת תווים ריקים מהקצוות של השורות
    return lines

def sort_lines(lines):
    words = []
    for line in lines:
        words += line.split() # המרת השורה לרשימת מילים
    words = list(set(words)) # מחיקת כפילויות
    words.sort() # מיון הרשימה לפי אלפבית
    for word in words:
        print(word)

def reverse_lines(lines):
    for line in lines:
        print(line[::-1])


def print_last_lines(lines, n):
    start_index = len(lines) - n
    for i in range(start_index, len(lines)):
        print(lines[i])

def action_select(lines, action):
    if action == "sort":
        sort_lines(lines)
    elif action == "rev":
        reverse_lines(lines)
    elif action == "last":
        n = int(input("Enter number of last lines to print: "))
        print_last_lines(lines, n)
    else:
        print("Invalid action")



"""
file_path = input("Enter file path: ")
action = input("Enter action (rev, sort, last): ")

lines = read_file(file_path)
action_select(lines, action)
"""

def copy_file_content(source, destination):
    content = ""
    with open(source, 'r') as read_f:
        content = read_f.read()
    
    with open(destination, 'w') as write_f:
        write_f.write(content)


copy_file_content("copy.txt", "paste.txt")
print("hello")

def who_is_missing(file_name):
    with open(file_name, 'r') as file:
        numbers = file.read().split(',')
        numbers = list(map(int, numbers))

    n = len(numbers) + 1
    expected_sum = n * (n + 1) // 2
    actual_sum = sum(numbers)
    missing_number = expected_sum - actual_sum

    with open('found.txt', 'w') as file:
        file.write(str(missing_number))

    return missing_number

print(who_is_missing("findMe.txt"))




def my_mp3_playlist(file_path):
    longest_song_name = ""
    num_of_songs = 0
    artist_dict = {}

    with open(file_path, 'r') as file:
        for line in file:
            # Split line by semicolon separator
            song_data = line.strip().split(";")

            # Extract song name, artist name, and duration
            song_name = song_data[0]
            artist_name = song_data[1]
            duration = song_data[2]

            # Check if song name is longer than current longest song name
            if len(song_name) > len(longest_song_name):
                longest_song_name = song_name

            # Increment number of songs
            num_of_songs += 1

            # Add artist to dictionary or increment their count
            if artist_name not in artist_dict:
                artist_dict[artist_name] = 1
            else:
                artist_dict[artist_name] += 1

    # Get artist name with maximum count
    max_artist_name = max(artist_dict, key=artist_dict.get)

    # Return a tuple of the three analysis results
    return (longest_song_name, num_of_songs, max_artist_name)



print(my_mp3_playlist("songs.txt"))


def my_mp4_playlist(file_path, new_song):
    with open(file_path, 'r') as f:
        lines = f.readlines()

    if len(lines) < 3:
        lines += ["\n"] * (3 - len(lines)) # Add empty lines if file has less than 3 lines

    # Extract song name from relevant line and replace with new song name
    song_line = lines[2].strip().split(";")
    song_line[0] = new_song
    lines[2] = ";".join(song_line) + "\n"

    with open(file_path, 'w') as f:
        f.writelines(lines)

    with open(file_path, 'r') as f:
        print(f.read())


my_mp4_playlist("songs.txt", "Python Love Story")


