import math

def squared_numbers(start, stop):
    list = []
    while start != stop:
        number = math.pow(start, 2)
        list.append(number) 
        start +=1
    return list


print(squared_numbers(4, 8))
print(squared_numbers(-3, 3))

def is_greater(my_list, n):
    greater = []
    for number in my_list:
        if number > n:
            greater.append(number)

    return greater


result = is_greater([1, 30, 25, 60, 27, 28], 28)
print(result)


def numbers_letters_count(my_str):
    cnt_letter = 0
    cnt_digits = 0
    for letter in my_str:
        if letter.isnumeric():
            cnt_digits +=1
        else:
            cnt_letter +=1

    list = [cnt_digits, cnt_letter]
    return list

print(numbers_letters_count("Python 3.6.3"))


def seven_boom(end_number):
    list = []
    for i in range(0, end_number + 1):
        if i % 7 == 0 or '7' in str(i):
            list.append("BOOM")
        else:
            list.append(i)

    return list

print(seven_boom(17))



def sequence_del(my_str):
    result = ""
    prev_char = ""
    for char in my_str:
        if char != prev_char:
            result += char
        prev_char = char
    return result

    

print(sequence_del("Heeyyy   yyouuuu!!!"))


def shopping_list(list):
    num = 0
    splitted_list = list.split(',')
    num = input("Please enter a number")
    while num != 9:
        if num == 1:
            print(list)
        elif num == 2:
            print(len(splitted_list))
        elif num == 3:
            exist = input("enter a product")
            print(exist in list)
        elif num == 4:
            product = input("enter a product")
            cnt = splitted_list.count(product)
            print(cnt)
        elif num == 5:
            product = input("enter a product")
            list = list.replace(product + ',', "", 1)
        elif num == 6:
            product = input("enter a product")
            (product + ',').join(list)
        elif num == 7:
            illegal = []
            for product in splitted_list:
                if len(product) < 3 or product.isalpha() == False:
                    illegal.append(product)

            print(product)
        elif num == 8:
            new_list = set(splitted_list)
            s =  ""
            ','.join(new_list)
            list = s
        
        num = input("Please enter a number")


def arrow(char, size):
    arrow_str = ""
    for i in range(1, size+1):
        arrow_str += char*i + "\n"
    for i in range(size-1, 0, -1):
        arrow_str += char*i + "\n"
    #arrow_str += char*(size+1) + "\n"
    return arrow_str

print(arrow('*', 5))



